export default function City({ name, codepostal }) {
	return (<div className="flex flex-row  justify-between bg-blue-300 hover:bg-blue-200 p-3 border-2 rounded-md border-blue-400 text-black">
		<span>{name}</span>
		<span>{codepostal}</span>
	</div>)
}