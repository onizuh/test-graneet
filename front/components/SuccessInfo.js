export default function Info({ count }) {
  const isSuccess = count > 0 ? 'green': 'red'
  const message =  isSuccess === 'green' ? `${count} villes correspondant au texte saisi` : 'Aucune villes correpondant au texte saisi'
  return (
    <div className={`flex flex-row  justify-between bg-${isSuccess}-300 hover:bg-${isSuccess}-200 mx-5 p-3 border-2 rounded-md border-${isSuccess}-400 text-black`}>
      {message}
    </div>
  )
}