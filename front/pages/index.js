import { useState } from "react";
import useSWR from 'swr'
import City from "../components/City";
import SuccessInfo from "../components/SuccessInfo";

const fetcher = (url) => fetch(url).then((res) => res.json())
export default function Home() {
  const [searchTerm, setSearchTerm] = useState(null)
  const { data, error } = useSWR(
    () => (searchTerm ? `http://localhost:3000/api/city?search=${searchTerm}` : null),
    fetcher)

  const handleChange = (event) => {
    setSearchTerm(event.target.value)
  }

  return (
    <div className="flex flex-col min-h-full p-5 bg-grey-50">
      <div className="flex fex-row text-2xl">
        <p className="flex-initial">Je recherche ...</p>
        <input className="flex-auto ml-4 focus:ring-2 focus:ring-blue-600 border-2 rounded-md border-blue-300" onChange={handleChange} />
      </div>

      <div className="flex fex-col text-2xl">
        <div>{data?.total} -- </div>
        <div>{data?.last_page}</div>
      </div>

      <div className="flex fex-row text-md mt-5 gap-5 h-full">
        <div className="flex flex-col bg-blue-100 w-6/12 h-full rounded-md">
          <p className="text-center p-5 text-xl">Villes de metropole</p>
          {searchTerm && <SuccessInfo count={data?.results?.inMetropole?.length} />}
          <div className="grid grid-cols-2 gap-4 p-5">
            {data?.results?.inMetropole?.map((city) => (<City key={city.id} name={city.name} codepostal={city.codepostal} />))}
          </div>
        </div>
        <div className="flex flex-col bg-blue-100 w-6/12 h-full rounded-md">
          <p className="text-center p-5 text-xl">Villes d'outre-mer</p>
          {searchTerm && <SuccessInfo count={data?.results?.outOfMetropole?.length} />}
          <div className="grid grid-cols-2 gap-4 p-5">
            {data?.results?.outOfMetropole?.map((city) => (<City key={city.id} name={city.name} codepostal={city.codepostal} />))}
          </div>
        </div>
      </div>
    </div>
  )
}