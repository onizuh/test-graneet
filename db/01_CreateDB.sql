CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

SELECT uuid_generate_v4();

CREATE TABLE city (
    id uuid DEFAULT uuid_generate_v4(),
    codePostal        char(5) NOT NULL,
    "name"              varchar(50),
    isinmetropol        boolean
);

CREATE INDEX cityName_idx ON city("name");