import { Controller, Get, Req } from '@nestjs/common';
import { CityEntity } from 'src/entity/city.entity';
import { Request } from "express";
import { CityService } from './city.service';

@Controller('city')
export class CityController {
    constructor(
        private cityService: CityService,
    ) {}

    @Get()
    async search(@Req() req: Request) {
        const builder = await this.cityService.queryBuilder('city');

        if (req.query.search) {
            builder.where("city.name LIKE :search ", { search: `%${req.query.search}%` })
        }

        const page: number = parseInt(req.query.page as any) || 1
        const perPage = 100;
        const total = await builder.getCount();
        builder.offset((page -1) * perPage).limit(perPage)

        let results = await builder.getMany()
        results = results.reduce((prev: any, curr: any) => {
            if(curr.isinmetropol) {
              return {...prev, inMetropole: prev.inMetropole ? [...prev.inMetropole ,curr] : [curr]}
            }
            return {...prev, outOfMetropole: prev.outOfMetropole ? [...prev.outOfMetropole, curr]: [curr]}
        },{})


        return {
            results,
            total,
            last_page: Math.ceil(total / perPage)
        }
    }

}
