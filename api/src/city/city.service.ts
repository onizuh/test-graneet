import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { CityEntity } from 'src/entity/city.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CityService {
    constructor(
        @InjectRepository(CityEntity) private readonly cityRepository: Repository<CityEntity>
    ) {
    }

    async all(): Promise<CityEntity[]> {
        return await this.cityRepository.find();
    }

    async queryBuilder(alias: string) {
        return this.cityRepository.createQueryBuilder(alias);
    }

}
