import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity('city')
export class CityEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column("varchar", { length: 5 })
    codepostal: string;

    @Column("varchar", { length: 50 })
    name: string;

    @Column("boolean", { default: false })
    isinmetropol: string;

}